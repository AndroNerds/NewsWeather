package com.andronerds.newsweather.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andronerds.newsweather.R
import com.andronerds.newsweather.adapters.NewsAdapter
import com.andronerds.newsweather.model.News
import com.andronerds.newsweather.service.ApiClient
import com.andronerds.newsweather.service.ApiInterface
import com.andronerds.newsweather.utils.PrefsUtil
import com.andronerds.newsweather.utils.Util
import kotlinx.android.synthetic.main.fragment_news.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Hemant on 23-05-2017.
 */
class NewsFragment : Fragment() {
    private val API_KEY: String = ""

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_news, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        newsRecycler.layoutManager = LinearLayoutManager(context)

        val responseCall = ApiClient().getClient()!!.create(ApiInterface::class.java)

        val source = PrefsUtil().NewsSources(context)
        val newsSource = Util().NewsSources(context, source!!)

        val type = PrefsUtil().NewsType(context)
        val newsType = Util().NewsType(context, type!!)

        val call = responseCall.NewsDetail(newsSource!!, newsType!!, API_KEY)
        Log.e("URL", call.toString())
        val response = call.enqueue(object: Callback<News> {
            override fun onResponse(call: Call<News>?, response: Response<News>?) {
                val news = response?.body()
                newsRecycler.adapter = NewsAdapter(news!!, context)
            }

            override fun onFailure(call: Call<News>?, t: Throwable?) {
                Log.e("Error", t.toString())
            }
        })
    }
}