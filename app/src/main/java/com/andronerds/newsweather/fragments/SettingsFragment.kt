package com.andronerds.newsweather.fragments

import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import com.andronerds.newsweather.R

/**
 * Created by Hemant on 24-05-2017.
 */
class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(p0: Bundle?, p1: String?) {
        setPreferencesFromResource(R.xml.settings, p1)
    }
}