package com.andronerds.newsweather.service

import com.google.gson.Gson
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by Hemant on 24-05-2017.
 */
class ApiClient {
    private val NEWS_URL: String? = "https://newsapi.org/v1/"
    private var retrofit: Retrofit? = null
    private var apiClient: ApiClient? = null

    fun getClient() : Retrofit? {
        retrofit = Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create())
                .baseUrl(NEWS_URL)
                .build()

        return retrofit
    }
}