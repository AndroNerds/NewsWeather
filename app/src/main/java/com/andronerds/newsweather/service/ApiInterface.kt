package com.andronerds.newsweather.service

import com.andronerds.newsweather.model.News
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Hemant on 24-05-2017.
 */
interface ApiInterface {
    @GET("articles")
    fun NewsDetail(@Query("source") source: String, @Query("sortBy") sortBy: String, @Query("apiKey") apiKey: String) : Call<News>
}