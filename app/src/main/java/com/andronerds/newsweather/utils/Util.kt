package com.andronerds.newsweather.utils

import android.content.Context
import com.andronerds.newsweather.R

/**
 * Created by Hemant on 25-05-2017.
 */
class Util() {

    fun NewsSources(context: Context, source: String): String? {
        var newsResponse: String? = null

        when(source) {
            context.getString(R.string.default_news_source) -> {
                newsResponse = context.getString(R.string.default_news_source)
            }
            context.getString(R.string.bbc) -> {
                newsResponse = context.getString(R.string.bbc)
            }
            context.getString(R.string.bloomberg) -> {
                newsResponse = context.getString(R.string.bloomberg)
            }
            context.getString(R.string.cnn) -> {
                newsResponse = context.getString(R.string.cnn)
            }
            context.getString(R.string.espn) -> {
                newsResponse = context.getString(R.string.espn)
            }
            context.getString(R.string.hacker) -> {
                newsResponse = context.getString(R.string.hacker)
            }
            context.getString(R.string.mirror) -> {
                newsResponse = context.getString(R.string.mirror)
            }
            context.getString(R.string.natgeo) -> {
                newsResponse = context.getString(R.string.natgeo)
            }
            context.getString(R.string.polygon) -> {
                newsResponse = context.getString(R.string.polygon)
            }
            context.getString(R.string.reddit) -> {
                newsResponse = context.getString(R.string.reddit)
            }
            context.getString(R.string.techcrunch) -> {
                newsResponse = context.getString(R.string.techcrunch)
            }
            context.getString(R.string.techradar) -> {
                newsResponse = context.getString(R.string.techradar)
            }
            context.getString(R.string.toi) -> {
                newsResponse = context.getString(R.string.toi)
            }
            context.getString(R.string.time) -> {
                newsResponse = context.getString(R.string.time)
            }
        }
        return newsResponse
    }

    fun NewsType(context: Context, type: String): String? {
        var newsType: String? = null

        when(type) {
            context.getString(R.string.default_news_type) -> {
                newsType = context.getString(R.string.default_news_type)
            }
            context.getString(R.string.latest) -> {
                newsType = context.getString(R.string.latest)
            }
        }
        return newsType
    }
}