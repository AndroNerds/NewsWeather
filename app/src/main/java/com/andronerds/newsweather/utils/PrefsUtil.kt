package com.andronerds.newsweather.utils

import android.content.Context
import android.preference.PreferenceManager
import com.andronerds.newsweather.R

/**
 * Created by Hemant on 24-05-2017.
 */
class PrefsUtil {
    private val NEWS_SOURCE: String = "pref_news_source"
    private val NEWS_TYPE: String = "pref_news_type"

    fun NewsSources(context: Context): String? {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString(NEWS_SOURCE, context.getString(R.string.default_news_source))
    }

    fun NewsType(context: Context): String? {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString(NEWS_TYPE, context.getString(R.string.default_news_type))
    }
}