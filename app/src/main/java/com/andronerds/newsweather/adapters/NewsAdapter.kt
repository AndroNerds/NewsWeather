package com.andronerds.newsweather.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.transition.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.andronerds.newsweather.R
import com.andronerds.newsweather.model.Article
import com.andronerds.newsweather.model.News
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.news_list.view.*
import kotlinx.android.synthetic.main.news_source.view.*
import java.lang.Exception

/**
 * Created by Hemant on 23-05-2017.
 */

class NewsAdapter(val news: News, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), RequestListener<String, GlideDrawable> {
    private val TYPE_HEADER: Int? = 0
    private val TYPE_VIEW: Int? = 1

    private var newsHolder: NewsViewHolder? = null

    fun Item(position: Int) : Article? {
        return news.articles[position - 1]
    }

    override fun onCreateViewHolder(p0: ViewGroup?, p1: Int): RecyclerView.ViewHolder? {
        if (p1 == TYPE_HEADER) {
            val view = LayoutInflater.from(p0?.context).inflate(R.layout.news_source, p0, false)
            return NewsSource(view)
        }
        else if (p1 == TYPE_VIEW) {
            val view = LayoutInflater.from(p0?.context).inflate(R.layout.news_list, p0, false)
            return NewsViewHolder(view)
        }
        return null
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder?, p1: Int) {
        if (p0 is NewsSource) {
            val holder = p0 as NewsSource

            holder.newsSource.text = news.source
        }
        else if (p0 is NewsViewHolder) {
            newsHolder = p0 as NewsViewHolder
            val model = Item(p1)

            Glide.with(context)
                    .load(model?.urlToImage)
                    .listener(this)
                    .into(newsHolder!!.newsImg)

            newsHolder!!.header.text = model?.title
            newsHolder!!.detail.text = model?.description
        }
    }

    override fun getItemCount(): Int {
        return news.articles.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        if (isPositionHeader(position)!!) {
            return TYPE_HEADER!!
        }
        return TYPE_VIEW!!
    }

    fun isPositionHeader(position: Int) : Boolean? {
        return position == 0
    }

    override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
        newsHolder!!.progress.visibility = CustomProgress.GONE
        return false
    }

    override fun onException(e: Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
        newsHolder!!.progress.visibility = CustomProgress.GONE
        return false
    }

    class NewsViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val newsImg: ImageView by lazy(LazyThreadSafetyMode.NONE) { itemView!!.iv_news }
        val progress: ProgressBar by lazy(LazyThreadSafetyMode.NONE) { itemView!!.progressBar }
        val header: TextView by lazy(LazyThreadSafetyMode.NONE) { itemView!!.tv_header }
        val detail: TextView by lazy(LazyThreadSafetyMode.NONE) { itemView!!.tv_detail }
    }

    class NewsSource(view: View?) : RecyclerView.ViewHolder(view) {
        val newsSource: TextView by lazy(LazyThreadSafetyMode.NONE) { view!!.news_source }
    }

    class CustomProgress(context: Context?) : View(context) {
        companion object {
            val GONE = 1
            val VISIBLE = 2
            val INVISIBLE = 3
        }
    }
}