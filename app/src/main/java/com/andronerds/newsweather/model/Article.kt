package com.andronerds.newsweather.model

/**
 * Created by Hemant on 24-05-2017.
 */
data class Article(
        val title: String,
        val description: String,
        val url: String,
        val urlToImage: String
)