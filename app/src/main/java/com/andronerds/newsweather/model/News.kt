package com.andronerds.newsweather.model

/**
 * Created by Hemant on 23-05-2017.
 */
data class News(
        val source: String,
        val articles: List<Article>
)
